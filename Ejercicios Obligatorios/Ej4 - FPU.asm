%include "io.inc"
extern _printf
section .data
vector dd 25.36,12.2,411.2,9.12
cantidad dd 4
result dq 0.0
formato db "El resultado es : %f", 10,13,0
section .text
global CMAIN
CMAIN:
    mov ebp, esp; for correct debugging
    xor ebx,ebx ;lo limpio para usarlo luego - ebx actuara como contador en el ciclo
    push cantidad
    push vector
    call suma_vf
    add esp,8 ;sumo 8 porque cargue en la pila dos words (4 bytes)
    
    ;Imprimo por salida
    push dword[result+4]
    push dword[result]  
    push formato
    call printf 
    add esp,12 ;sumo 12 porque cargue en la pila tres words (4 bytes)
    
    xor eax, eax

    ret
    
suma_vf:
    
    push ebp ; enter
    mov ebp, esp  ; enter
    
    mov eax , [EBP+8] ;en eax tengo la direccion del vector
    mov edx , [EBP+12];en edx tengo la direccion de la cantidad de elementos
    
    ;cargo el primer valor para poder ir sumandolo 
    ;con el segundo valor en el ciclo
    FLD dword[eax]
    inc ebx ;incremento en uno el contador
    
ciclo: 
    FLD dword[eax +4*ebx]
    FADDP ;sumo los dos primeros elementos de la pila
    inc ebx ;incremento en uno el contador
    
    cmp ebx, [edx]
    jl ciclo ;si es menor a la cantidad sigo en el ciclo
    
    ;fin del ciclo------------
 
    fst qword[result] ;guardo el valor en la var result
    
    mov ebp,esp ;Reset Stack (leave)
    pop ebp ;Restore (leave)
    
    ret