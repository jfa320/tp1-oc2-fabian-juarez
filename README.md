# TP1 - OC2 :computer:

## Descripción 📖
El presente repositorio contiene tanto los ejercicios obligatorios como el codigo en C y Assembler funcionando como debe. 
Se pedia hacer una función en assembler que resuelva la formula resolvente, calculando ambas raices asumiendo que:


* b^2 - 4ac >= 0, ∀ a, b, c ε R
* a > 0

Luego llamar esa función desde C. En C pedir parámetros de a, b y c al usuario.

## Ejecución Formula Resolvente :boom:

1. Primero se aclara que se uso un [script bash](/script.sh) para evitar escribir tantas veces lo mismo por consola.
Se observa a continuación:

![script.sh](Capturas/1.png)

Donde:
* La primera línea convierte el asm a un objeto
* La segunda línea linkea el objeto antes generado para luego compilar el código C. Obtenemos un ejecutable
* La tercera simplemente ejecuta el compilado para dar inicio al programa

2. **Para ejecutar simplemente correr ese script por la terminal como se muestra a continuación:**

![script.sh](Capturas/2.png)

El código para copiar y pegar la linea a ejecutar es:

```
sudo sh script.sh
```
_No olvidar ingresar su contraseña de root user al usar sudo_

3. El programa ya está iniciado! Ahora ingresar valores que cumplan lo descripto en la Descripción para verlo en marcha:

![script.sh](Capturas/3.png)
<foto3>

## Ejercicios Obligatorios :bulb:

Se adjuntan:
* [Ejercicio 4](Ejercicios Obligatorios/Ejercicios Gestión de Memoria - Juarez.pdf) - Gestión de memoria. 
* [Ejercicio 6](Ejercicios Obligatorios/Ejercicios Gestión de Memoria - Juarez.pdf) - Gestión de memoria.
* [Ejercicio 7](Ejercicios Obligatorios/Ejercicios Gestión de Memoria - Juarez.pdf) - Gestión de memoria.
* [Ejercicio 4](Ejercicios Obligatorios/Ej4 - FPU.asm) - FPU.

## Autor ✒️

* **Fabián Juárez** - [jfa320](https://github.com/jfa320)
