extern printf
global resolvente

section .data

;variables principales.
parcial0 dd 0.0 ;aca guardo -b
parcial1 dd 0.0 ;aca guardo el resultado parcial de: b elevado a la 2
parcial2 dd 0.0 ;aca guardo el resultado parcial de: 4ac
parcial3 dd 0.0 ;aca guardo el resultado parcial de: 2a
parcialRaiz dd 0.0 ;guardo el resultado parcial de la raiz de la formula
parcialNumeradorSuma dd 0.0 ;para el numerador cuando suma
parcialNumeradorResta dd 0.0;para el numerador cuando resta

;Raices - resultados finales
raiz1 dq 0.0 ;guardo la raiz1
raiz2 dq 0.0 ;guardo la raiz2


;Los aux son simples valores constantes (aunque guardados como variables)
aux1 dd 4.0
aux2 dd 2.0

;El texto a imprimir
formato db "La raiz 1 es: %.2f y la raiz 2 es: %.2f  ", 10,13,0

section .text 
 
resolvente:
    push ebp ; enter
    mov ebp, esp  ; enter
    
    ;variables que vienen de C:
    ;a=EBP+8
    ;b=EBP+12
    ;c=EBP+16
    
    ;calculo -B
    FLD dword[EBP+12]
    FCHS ;cambio el signo por el opuesto
    FSTP dword[parcial0] ;guardo en variable y hago un pop   
    
    ;calculo B al cuadrado (multiplico los mismos valores de la pila entre si)
    FLD dword[EBP+12]
    FLD dword[EBP+12]
    FMUL ;multiplicacion entre ambos valores
    FSTP dword[parcial1] ;guardo en variable y hago un pop
    
    ;calculo 4*a*c
    FLD dword[aux1] ;cargo el valor 4 en la pila
    FLD dword[EBP+8] ;cargo variable a
    FMUL ;multiplico
    FLD dword[EBP+16] ;cargo variable c en pila
    FMUL ;multiplico
    FSTP dword[parcial2]  ;guardo en variable y hago un pop  
    
    ;calculo 2*a (divisor)
    FLD dword[aux2] ;cargo el valor 2 en la pila
    FLD dword[EBP+8];cargo el valor a en la pila
    FMUL ;multiplico ambos valores
    FSTP dword[parcial3] ;guardo en variable y hago un pop
    
    ;calculo Raiz cuadrada de la resta de: B al cuadrado - 4*a*c
    FLD dword[parcial1]    ;cargo B al cuadrado
    FLD dword[parcial2]    ;cargo 4*a*c
    FSUB ;hago la resta
    FSQRT ;al resultado de la resta,le calculo la raiz cuadrada
    FSTP dword[parcialRaiz] ;guardo en variable y hago un pop
    
    ;calculo Numerador Suma 
    FLD dword[parcial0] ;cargo -b en la pila
    FLD dword[parcialRaiz] ;cargo el resultado de la raiz antes calculado
    FADD  ;hago la suma
    FSTP dword[parcialNumeradorSuma] ;guardo en variable y hago un pop
    
    ;calculo Numerador Resta
    FLD dword[parcial0] ;cargo -b en la pila
    FLD dword[parcialRaiz] ;cargo el resultado de la raiz antes calculado
    FSUB ;hago la resta
    FSTP dword[parcialNumeradorResta] ;guardo en variable y hago un pop
    
    ;calculo divisiones entre los numeradores calculados antes y el divisor
    ;luego guardo Raices en su respectiva variable
    
    FLD dword[parcialNumeradorSuma]
    FLD dword[parcial3] 
    FDIV ;hago: parcialNumeradorSuma/parcial3
    FSTP qword[raiz1] ;guardo en variable y hago un pop
    FLD dword[parcialNumeradorResta]
    FLD dword[parcial3] 
    FDIV ;hago: parcialNumeradorResta/parcial3
    FSTP qword[raiz2] ;guardo en variable y hago un pop
    
    ;Imprimo por salida
    push dword[raiz2+4]
    push dword[raiz2]
    push dword[raiz1+4]
    push dword[raiz1]
    push formato
    call printf 
    add esp,20 ;sumo 20 porque cargue en la pila cinco words (4 bytes)
    
    
    mov ebp,esp ;Reset Stack (leave)
    pop ebp ;Restore (leave)
    
    ret
