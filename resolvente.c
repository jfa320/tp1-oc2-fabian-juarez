#include <stdio.h>
#include <stdlib.h>

extern void resolvente(float a,float b,float c);

int main() {
	
	float a,b,c;
	//Pido los coeficientes
	printf("Introduzca el coeficiente A: ");
    scanf("%f", &a);
	printf("Introduzca el coeficiente B: ");
    scanf("%f", &b);
	printf("Introduzca el coeficiente C: ");
    scanf("%f", &c);
	//Llamo a la función hecha en assembler
	resolvente(a,b,c);
	return 0;
}